#ifndef CONSTANTS_H
#define CONSTANTS_H

// Debugging
const bool debug = true;
const bool kPlotVectorField = false;

// Size constants
const int kEyePercentTop = 25;
const int kEyePercentSide = 13;
const int kEyePercentHeight = 30;
const int kEyePercentWidth = 35;
const float kNosePercentTop = 0.2;

// Preprocessing
const bool kSmoothFaceImage = false;
const float kSmoothFaceFactor = 0.002;

// Algorithm Parameters
const int kFastEyeWidth = 50;
const int kWeightBlurSize = 5;
const bool kEnableWeight = true;
const float kWeightDivisor = 1.0;
const double kGradientThreshold = 80.0;//50

// Postprocessing
const bool kEnablePostProcess = true;
const float kPostProcessThreshold = 0.97;//.97

// Eye Corner
const bool kEnableEyeCorner = false;

//step recognizing
const float thXratio = 0.069;

// hmd
const int kDefaultWinSize = 31;
const float thHmdRatio = 0.007; //0.005 high sensitive
const int kDefaultStep = 15;
const int kDefaultThBpm = (int)kDefaultStep*0.60;
const int kDefaultBpmPrebuffer = (int)kDefaultThBpm*1.4;
const int minBPM = 170;
const int maxBPM = 400;

//excercise 1
const int rowD = 7;
const int colD = 4;
const float offsetPerc = 0.05;
const int setStartBPM = 185;
#endif