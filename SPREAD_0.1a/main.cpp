#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/System.hpp>

#include <iostream>
#include <fstream>
#include <queue>
#include <stdio.h>
#include <math.h>
#include <ctime>
#include <array>

#include "constants.h"
#include "HMD.h"
#include "findEyeCenter.h"



/** Constants **/

typedef enum SoundType{Metronome, Buzz,SpeedUp,SpeedDown}soundtype_e;
/** Function Headers */
cv::Rect detectAndDisplay(cv::Mat frame,bool);
void drawSpreadWindow(sf::RenderWindow *);
int playSound(soundtype_e type);
bool checkPupilStep(cv::Point , cv::Rect );
bool checkPupilStep2(cv::Point, cv::Rect);
/** Global variables */
//-- Note, either copy these two files from opencv/data/haarscascades to your current folder, or change these locations
cv::String face_cascade_name = "haarcascade_frontalface_alt.xml";
cv::CascadeClassifier face_cascade;
std::string main_window_name = "Capture - Face detection";
std::string face_window_name = "Capture - Face";
cv::RNG rng(12345);
cv::Mat debugImage;
cv::Mat skinCrCbHist = cv::Mat::zeros(cv::Size(256, 256), CV_8UC1);

cv::Point2f stepTresholdR = cv::Point2f(100.0, 100.0);
cv::Point plpR;


sf::Clock cf,fpsC;
int BPM=setStartBPM;
sf::Mutex bpm_mutex, ada_mutex;
SpeedAdaption adapt;

bool needToInit = true;
/**
* @function main
*/
int main(int argc, const char** argv) {
	sf::Sound sound;
	sf::SoundBuffer buf;
	buf.loadFromFile("buzz.wav");
	sound.setBuffer(buf);
    float fps;
	cv::Mat frame;
	HMD hmd;
	bool grayscaleOn = false;
	bool isFirstFrame = true;
    sf::VideoMode desktop = sf::VideoMode::getDesktopMode();
    sf::RenderWindow window(sf::VideoMode(desktop.width/2, desktop.height/2, 8), "SPREAD", sf::Style::Fullscreen);
    
	sf::Thread spreadWindow(&drawSpreadWindow, &window);
	adapt = SpeedAdaption(&BPM, kDefaultBpmPrebuffer, kDefaultStep, kDefaultThBpm);
	cv::Size subPixWinSize(10, 10), winSize(31, 31);
	
	// Load the cascades
	if (!face_cascade.load(face_cascade_name)){ printf("--(!)Error loading face cascade, please change face_cascade_name in source code.\n"); return -1; };

	cv::namedWindow(face_window_name, CV_WINDOW_NORMAL);
	cv::moveWindow(face_window_name, 200, 200);
	/*cv::namedWindow(main_window_name, CV_WINDOW_NORMAL);
	cv::moveWindow(main_window_name, 400, 100);
	
	cv::namedWindow("Right Eye", CV_WINDOW_NORMAL);
	cv::moveWindow("Right Eye", 10, 600);
	cv::namedWindow("Left Eye", CV_WINDOW_NORMAL);
	cv::moveWindow("Left Eye", 10, 800);
	cv::namedWindow("aa", CV_WINDOW_NORMAL);
	cv::moveWindow("aa", 10, 800);
	cv::namedWindow("aaa", CV_WINDOW_NORMAL);
	cv::moveWindow("aaa", 10, 800);
	*/
	ellipse(skinCrCbHist, cv::Point(113, 155.6), cv::Size(23.4, 15.2),
		43.0, 0.0, 360.0, cv::Scalar(255, 255, 255), -1);

	// Read the video stream
	cv::VideoCapture cap(0);
	if (!cap.isOpened())
	{
		std::cout << "Could not initialize capturing...\n";
		return 0;
	}
	// Launch excercise window
    fpsC.restart();
	spreadWindow.launch();
	while (true) {
        sf::Event event;
        while (window.pollEvent(event))
        {
            // "close requested" event: we close the window
            if (event.type == sf::Event::Closed || sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
                window.close();
            return 0;
        }
		cap >> frame;

		// mirror it
		cv::flip(frame, frame, 1);
		frame.copyTo(debugImage);
		// Apply the classifier to the frame
		if (!frame.empty()) {
			if (!isFirstFrame){
				detectAndDisplay(frame, grayscaleOn);
				if (hmd.checkMovement(frame)){
					sound.play();
                   // printf("Head Movement Detected!\n");
				}
				
			}
			else{
				hmd.init(frame, detectAndDisplay(frame, grayscaleOn));
				isFirstFrame = false;
			}
		}
		else {
			printf(" --(!) No captured frame -- Break!");
			break;
		}
		if (cf.getElapsedTime().asSeconds() > 10){
			isFirstFrame = true;
			cf.restart();
		}
		// imshow(main_window_name, debugImage);
		
		int c = cv::waitKey(10);
		if ((char)c == 'q') { exit(1); }
		if ((char)c == 'f') {
			imwrite("frame.png", frame);
		}
		if ((char)c == 'g'){
			grayscaleOn = !grayscaleOn;
		}
	}



	return 0;
}
void drawSpreadWindow(sf::RenderWindow *window){
	sf::Color DOT_OFF = sf::Color::Black; DOT_OFF.a =40;
	sf::Color DOT_ON = sf::Color::Blue;
	
	const int nDots     = colD * rowD;
	std::array<sf::CircleShape,nDots> dots;
	//sf::Clock clock;
	// sf::Time elapsed = clock.getElapsedTime();
	sf::Sound sound, soundSpeed;
	sf::SoundBuffer buf,bufSU,bufSD;
	buf.loadFromFile("./click.wav");
    bufSU.loadFromFile("./speed_up.wav");
    bufSD.loadFromFile("./speed_down.wav");
	sound.setBuffer(buf);

	//prepare gray dots
	
	ada_mutex.lock();
	adapt.reset(kDefaultBpmPrebuffer/2);
	ada_mutex.unlock();
	int i = 0;
	while (window->isOpen()){
		
		
		window->setFramerateLimit(12);
		window->clear(sf::Color::White);
		for (int x = 0, y = 0, k = 0; k<dots.size(); k++){
			dots[k].setPosition(sf::Vector2f(x+(window->getSize().x*offsetPerc), y));
			dots[k].setFillColor(DOT_OFF);
			dots[k].setRadius(window->getSize().y/(3*rowD));
			x += window->getSize().x / colD;
			x %= window->getSize().x;
			if (x == 0){
				y += window->getSize().y / rowD;
				y %= window->getSize().y;
			}
			window->draw(dots[k]);
		}
		dots[i].setFillColor(DOT_ON);
		window->draw(dots[i]);
		window->display();
		if (++i%nDots == 0) i = 0;
		//playSound(Metronome);
		ada_mutex.lock();
		adapt.coundDec();
		ada_mutex.unlock();
		sound.play();
		bpm_mutex.lock();
		long interval = (60.0f / (float)BPM) * 1000;
		if (BPM<20 && BPM >500) exit(-1);
		int sud=adapt.go();
		bpm_mutex.unlock();
        if (sud==1){
            soundSpeed.setBuffer(bufSU);
            soundSpeed.play();
        }
        else if (sud==-1){
            soundSpeed.setBuffer(bufSD);
            soundSpeed.play();
        }
		sf::sleep(sf::milliseconds(interval));
	}
}

int playSound(soundtype_e type){
	sf::SoundBuffer buffer;
	if (type == Buzz ){
		if(!buffer.loadFromFile("./buzz.wav"))
			return -1;
	}
	else if (type == Metronome){
		if(!buffer.loadFromFile("./click.wav"))
		return -1;
	}
	sf::Sound sound;
	sound.setBuffer(buffer);
	sound.play();
}



void findEyes(cv::Mat frame_gray, cv::Rect face) {
	cv::Mat faceROI = frame_gray(face);
	cv::Mat debugFace = faceROI;
	sf::SoundBuffer buffer;
	sf::Sound sound;
	if (kSmoothFaceImage) {
		double sigma = kSmoothFaceFactor * face.width;
		GaussianBlur(faceROI, faceROI, cv::Size(0, 0), sigma);
	}
	//-- Find eye regions and draw them
	int eye_region_width = face.width * (kEyePercentWidth / 100.0);
	int eye_region_height = face.width * (kEyePercentHeight / 100.0);
	int eye_region_top = face.height * (kEyePercentTop / 100.0);
	cv::Rect rightEyeRegion(face.width - eye_region_width - face.width*(kEyePercentSide / 100.0),eye_region_top, eye_region_width, eye_region_height);

	//-- Find Eye Centers

	cv::Point rightPupil = findEyeCenter(faceROI, rightEyeRegion, "Right Eye");
	//-- Find eye movement
	if (checkPupilStep(rightPupil, rightEyeRegion))
		ada_mutex.lock();
		adapt.countInc();
		ada_mutex.unlock();

	cv::Rect rightLeftCornerRegion(rightEyeRegion);
	rightLeftCornerRegion.width = rightPupil.x;
	rightLeftCornerRegion.height /= 2;
	rightLeftCornerRegion.y += rightLeftCornerRegion.height / 2;
	cv::Rect rightRightCornerRegion(rightEyeRegion);
	rightRightCornerRegion.width -= rightPupil.x;
	rightRightCornerRegion.x += rightPupil.x;
	rightRightCornerRegion.height /= 2;
	rightRightCornerRegion.y += rightRightCornerRegion.height / 2;

	rectangle(debugFace, rightLeftCornerRegion, 200);
	rectangle(debugFace, rightRightCornerRegion, 200);
	// change eye centers to face coordinates
	rightPupil.x += rightEyeRegion.x;
	rightPupil.y += rightEyeRegion.y;

	// draw eye centers
	circle(debugFace, rightPupil, 3, 1234);

	 imshow(face_window_name, faceROI);

}
bool checkPupilStep2(cv::Point pupilPos, cv::Rect rif){
	std::ofstream myfile;
	bool p = false;
	if (checkPupilStep(pupilPos, rif)){
		p = true;
		playSound(Buzz);
	}
	myfile.open("exampleR2.csv", std::ofstream::app);
	myfile << pupilPos.x << ";" << (p?plpR.x:0) << ";" << rif.width << "; " << cf.getElapsedTime().asMilliseconds() << "; " << std::endl;
	myfile.close();
	return true;
}
bool checkPupilStep(cv::Point pupilPos, cv::Rect rif){
	cv::Point2f distance = cv::Point2f(pupilPos.x, pupilPos.y);
	stepTresholdR.x = rif.width * thXratio;
	if (plpR.x>distance.x + stepTresholdR.x || plpR.x < distance.x - stepTresholdR.x){// || plpR.y>distance.y + stepTresholdR.y || plpR.y < distance.y - stepTresholdR.y){
		plpR.x = distance.x;
		return true;
		}
		//plpR.x = distance.x;
	return false;
}

/**
* @function detectAndDisplay
*/
cv::Rect detectAndDisplay(cv::Mat frame, bool grayscaleOn) {
	std::vector<cv::Rect> faces;
	//cv::Mat frame_gray;

	std::vector<cv::Mat> rgbChannels(3);
	cv::split(frame, rgbChannels);
	cv::Mat frame_gray = rgbChannels[2];
	//grayscale instead of Green channel
	if (grayscaleOn){
		cvtColor(frame, frame_gray, CV_BGR2GRAY);
		equalizeHist(frame_gray, frame_gray);
	}
	//-- Detect faces
	face_cascade.detectMultiScale(frame_gray, faces, 1.1, 2, 0 | CV_HAAR_SCALE_IMAGE | CV_HAAR_FIND_BIGGEST_OBJECT, cv::Size(150, 150));

	for (int i = 0; i < faces.size(); i++)
	{
		rectangle(debugImage, faces[i], 1234);
	}
	//-- Show what you got
	if (faces.size() > 0) {
		findEyes(frame_gray, faces[0]);
		return faces[0];
	}
}
