#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "HMD.h"
#include "constants.h"
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <ctype.h>


using namespace cv;
using namespace std;


HMD::HMD(int inWinSize){
		namedWindow("HMD", CV_WINDOW_AUTOSIZE);
		termcrit = TermCriteria(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 20, 0.03);
		subPixWinSize = Size(10, 10);
		winSize = Size(inWinSize, inWinSize);
		return;
}
HMD::HMD(){
	namedWindow("HMD", CV_WINDOW_NORMAL);
	termcrit = TermCriteria(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 20, 0.03);
	subPixWinSize = Size(10, 10);
	winSize = Size(31, 31);
	ref = Point2f(100,100);
	return;
}
void HMD::init(Mat newFrame,Rect face){
	if (called) return;
	newFrame.copyTo(image);
	cvtColor(image, gray, CV_BGR2GRAY);
	Point2f nosePosition(face.x+(face.width / 2), face.y+(face.height * kNosePercentTop));
	vector<Point2f> tmp;
	tmp.push_back(nosePosition);
	cornerSubPix(gray, tmp, winSize, cvSize(-1, -1), termcrit);

	points[1].push_back(tmp[0]);
	called = true;
}
bool HMD::checkMovement(Mat newFrame){
	bool moved = false;
	newFrame.copyTo(image);
	cvtColor(image, gray, CV_BGR2GRAY);
	if (!points[0].empty())
	{
		vector<uchar> status;
		vector<float> err;
		if (prevGray.empty())
			gray.copyTo(prevGray);
		calcOpticalFlowPyrLK(prevGray, gray, points[0], points[1], status, err, winSize,3, termcrit, 0, 0.001);
		size_t i, k;
		for (i = k = 0; i < points[1].size(); i++)
		{
			if (!status[i])
				continue;

			points[1][k++] = points[1][i];
			if (!OFThresolding(newFrame.size())){
				circle(image, points[1][i], 3, Scalar(0, 255, 0), -1, 8);
			}
			else{
				circle(image, points[1][i], 3, Scalar(0, 0, 255), -1, 8);
				moved = true;
			}
		}
		points[1].resize(k);
		
	}
	imshow("HMD", image);
	std::swap(points[1], points[0]);
	swap(prevGray, gray);

	return moved;
}
bool HMD::OFThresolding(Size frameSize){
	float rangeX = frameSize.width * thHmdRatio;
	float rangeY = frameSize.height *thHmdRatio;

	if (points[1][0].x > ref.x + rangeX || points[1][0].x<ref.x - rangeX
		|| points[1][0].y>ref.y + rangeY || points[1][0].y < ref.y - rangeY)
	{
		ref = points[1][0];
		return true;
	}
	return false;
}
HMD::~HMD(){
}
SpeedAdaption::SpeedAdaption(){
	threshold = 5;
	step = 5;
	this->value = nullptr;
}
SpeedAdaption::SpeedAdaption(int *value){
	threshold = 5;
	step = 5;
	this->value = value;
}
SpeedAdaption::SpeedAdaption(int *value, int start, int step, int threshold){
	effective = start;
	this->threshold = threshold;
	this->step = step;
	this->value = value;
}
int SpeedAdaption::countInc(){
	return effective+=2;
	//go();
}
int SpeedAdaption::coundDec(){ 
	return effective--;
	//go();
}
void SpeedAdaption::reset(){
	effective = 0;
}
void SpeedAdaption::reset(int v){
	effective = v;
}
void SpeedAdaption::setThreshold(int v){
	threshold = v;
}
void SpeedAdaption::setStep(int v){ step = v; }
int SpeedAdaption::go(){

	if (effective>2*threshold){
		*value += step;
		if (*value > max) *value = max;
		cout << "BPM ="<< *value << endl;
		reset(kDefaultBpmPrebuffer);
		return 1;
	}
	else if (effective<-threshold*0.7){
		*value -= step;
		if (*value < min) *value=min;
		cout << "BPM =" << *value << endl;
		reset(kDefaultBpmPrebuffer);
		return -1;
	}
	return 0;
}
SpeedAdaption::~SpeedAdaption(){
	value = nullptr;
}