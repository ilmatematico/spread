#ifndef HMD_c
#define HMD_c

#include "constants.h"
class HMD{
	bool called = false;
	cv::Mat gray, prevGray, image;
	cv::TermCriteria termcrit;
	cv::Size subPixWinSize, winSize;
	cv::vector<cv::Point2f> points[2];
	cv::Point2f ref;
	bool OFThresolding(cv::Size frameSize);
public:
	HMD();
	HMD(int);
	~HMD();
	void init(cv::Mat,cv::Rect face);
	bool checkMovement(cv::Mat newFrame); //function to call every frame
};
class SpeedAdaption{
	int effective = 0,threshold,*value,step, min=minBPM, max = maxBPM;
	
public:
	int go();
	SpeedAdaption();
	SpeedAdaption(int*);
	SpeedAdaption(int *value, int start, int step, int threshold);
	int countInc();
	int coundDec();
	void reset();
	void reset(int );
	void setThreshold(int );
	void setStep(int);
	~SpeedAdaption();

};
#endif